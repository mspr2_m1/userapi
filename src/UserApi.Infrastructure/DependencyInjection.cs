﻿using System.Text;
using System.Text.Json;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

using MsprApiUser.Application.Common.Interfaces.Authentication;
using MsprApiUser.Infrastructure.Authentication;

using UserApi.Application.Common.Interfaces.Authentication;
using UserApi.Application.Common.Interfaces.Persistence;
using UserApi.Infrastructure.Authentication;
using UserApi.Infrastructure.Persistence;
using UserApi.Infrastructure.Persistence.Interceptors;
using UserApi.Infrastructure.Persistence.Repositories;

namespace UserApi.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(
        this IServiceCollection services,
        ConfigurationManager configuration)
    {
        services
            .AddAuthentication(configuration)
            .AddPersistence(configuration);

        return services;
    }

    private static IServiceCollection AddPersistence(
        this IServiceCollection services,
        IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString("DefaultConnection");

        services.AddDbContext<UserApiDbContext>(options =>
            options.UseSqlServer(connectionString));

        services
            .AddScoped<PublishDomainEventsInterceptor>()
            .AddScoped<IUserRepository, UserRepository>()
            .AddScoped<IRoleRepository, RoleRepository>();

        return services;
    }

    private static IServiceCollection AddAuthentication(
        this IServiceCollection services,
        IConfiguration configuration)
    {
        JwtSettings jwtSettings = new();
        configuration.Bind(JwtSettings.SectionName, jwtSettings);

        services
            .AddSingleton(Options.Create(jwtSettings))
            .AddSingleton<IJwtTokenGenerator, JwtTokenGenerator>()
            .AddSingleton<IPasswordHasher, PasswordHasher>()
            .AddAuthentication(defaultScheme: JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.Events = new JwtBearerEvents
                {
                    OnChallenge = context =>
                    {
                        bool tokenMissing = string.IsNullOrEmpty(context.Request.Headers.Authorization);
                        context.Response.StatusCode = tokenMissing ? 401 : 403;
                        context.Response.ContentType = "application/json";

                        string message = tokenMissing
                            ? "Unauthorized: No token provided."
                            : "Access refused: you do not have the necessary rights for this operation.";

                        context.Response.WriteAsync(JsonSerializer.Serialize(new { message = message }));

                        context.HandleResponse();
                        return Task.CompletedTask;
                    },
                };

                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = jwtSettings.Issuer,
                    ValidAudience = jwtSettings.Audience,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Secret)),
                };
            });

        return services;
    }
}