﻿using Microsoft.EntityFrameworkCore;

using UserApi.Domain.Common.Models;
using UserApi.Domain.RoleAggregate;
using UserApi.Domain.UserAggregate;
using UserApi.Infrastructure.Persistence.Interceptors;

namespace UserApi.Infrastructure.Persistence;

public class UserApiDbContext(
    DbContextOptions<UserApiDbContext> options,
    PublishDomainEventsInterceptor publishDomainEventsInterceptor)
    : DbContext(options)
{
    public virtual DbSet<User> Users { get; set; } = null!;
    public virtual DbSet<Role> Roles { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .Ignore<List<IDomainEvent>>()
            .ApplyConfigurationsFromAssembly(typeof(UserApiDbContext).Assembly);

        base.OnModelCreating(modelBuilder);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.AddInterceptors(publishDomainEventsInterceptor);
        base.OnConfiguring(optionsBuilder);
    }
}