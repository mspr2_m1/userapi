﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using UserApi.Domain.RoleAggregate;
using UserApi.Domain.UserAggregate;
using UserApi.Domain.UserAggregate.ValueObjects;

namespace UserApi.Infrastructure.Persistence.Configurations;

public class UserConfigurations : IEntityTypeConfiguration<User>
{
    public void Configure(EntityTypeBuilder<User> builder)
    {
        ConfigureUserRolesTable(builder);
        ConfigureUserTable(builder);
    }

    private static void ConfigureUserRolesTable(EntityTypeBuilder<User> builder)
    {
        builder.HasMany(u => u.Roles)
            .WithMany()
            .UsingEntity(
                "UserRoles",
                e => e.HasOne(typeof(Role)).WithMany().HasForeignKey("RoleId"),
                e => e.HasOne(typeof(User)).WithMany().HasForeignKey("UserId"));
    }

    private static void ConfigureUserTable(EntityTypeBuilder<User> builder)
    {
        builder.ToTable("Users");

        builder.HasKey(u => u.Id);
        builder.Property(u => u.Id)
            .ValueGeneratedNever()
            .HasConversion(
                id => id.Value,
                value => UserId.Create(value));

        builder.Property(u => u.Email);
        builder.Property(u => u.Password);
        builder.Property(u => u.FirstName);
        builder.Property(u => u.LastName);
    }
}