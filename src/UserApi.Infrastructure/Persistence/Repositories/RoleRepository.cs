﻿using System.Data;

using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

using UserApi.Application.Common.Interfaces.Persistence;
using UserApi.Domain.RoleAggregate;
using UserApi.Domain.RoleAggregate.ValueObjects;

namespace UserApi.Infrastructure.Persistence.Repositories;

public class RoleRepository(UserApiDbContext dbContext) : IRoleRepository
{
    public async Task<List<Role>> GetAllAsync() =>
        await dbContext.Roles.ToListAsync();

    public async Task<Role?> GetByIdAsync(RoleId id) =>
        await dbContext.Roles.FirstOrDefaultAsync(r => r.Id == id);

    public async Task AddAsync(Role entity)
    {
        dbContext.Add(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task UpdateAsync(Role entity)
    {
        dbContext.Roles.Update(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(Role entity)
    {
        dbContext.Roles.Remove(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task<List<Role>> GetByIdsAsync(List<RoleId> roleIds)
    {
        var parameters = new SqlParameter("roleIds", SqlDbType.VarChar)
        {
            Value = string.Join(",", roleIds.Select(id => id.ToString())),
        };

        const string sqlQuery = "SELECT * FROM Roles WHERE Id IN (SELECT value FROM STRING_SPLIT(@roleIds, ','))";
        return await dbContext.Roles.FromSqlRaw(sqlQuery, parameters).ToListAsync();
    }
}