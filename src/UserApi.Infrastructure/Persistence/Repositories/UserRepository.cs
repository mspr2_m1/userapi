﻿using Microsoft.EntityFrameworkCore;

using UserApi.Application.Common.Interfaces.Persistence;
using UserApi.Domain.UserAggregate;
using UserApi.Domain.UserAggregate.ValueObjects;

namespace UserApi.Infrastructure.Persistence.Repositories;

public class UserRepository(UserApiDbContext dbContext) : IUserRepository
{
    public async Task<List<User>> GetAllAsync() =>
        await dbContext.Users.ToListAsync();

    public async Task<User?> GetByIdAsync(UserId id) =>
        await dbContext.Users
            .Include(u => u.Roles)
            .FirstOrDefaultAsync(u => u.Id == id);

    public async Task AddAsync(User entity)
    {
        dbContext.Add(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task UpdateAsync(User entity)
    {
        dbContext.Users.Update(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(User entity)
    {
        dbContext.Users.Remove(entity);
        await dbContext.SaveChangesAsync();
    }

    public async Task<User?> GetByEmailAsync(string email) =>
        await dbContext.Users
            .Include(u => u.Roles)
            .FirstOrDefaultAsync(u => u.Email == email);
}