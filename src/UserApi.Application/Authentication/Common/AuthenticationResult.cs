﻿using UserApi.Domain.UserAggregate;

namespace UserApi.Application.Authentication.Authentication.Common;

public record AuthenticationResult(
    User User,
    string Token);