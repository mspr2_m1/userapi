﻿using ErrorOr;

using MediatR;

using MsprApiUser.Application.Common.Interfaces.Authentication;

using UserApi.Application.Authentication.Authentication.Common;
using UserApi.Application.Common.Interfaces.Authentication;
using UserApi.Application.Common.Interfaces.Persistence;
using UserApi.Domain.Common.Errors;

namespace UserApi.Application.Authentication.Authentication.Queries.Login;

public class LoginQueryHandler(
    IUserRepository userRepository,
    IJwtTokenGenerator jwtTokenGenerator,
    IPasswordHasher passwordHasher)
        : IRequestHandler<LoginQuery, ErrorOr<AuthenticationResult>>
{
    public async Task<ErrorOr<AuthenticationResult>> Handle(
        LoginQuery query,
        CancellationToken cancellationToken)
    {
        if (await userRepository.GetByEmailAsync(query.Email) is not { } user)
        {
            return Errors.Authentication.InvalidCredentials;
        }

        if (!passwordHasher.VerifyPassword(query.Password, user.Password))
        {
            return Errors.Authentication.InvalidCredentials;
        }

        return new AuthenticationResult(
            user,
            jwtTokenGenerator.GenerateToken(user));
    }
}