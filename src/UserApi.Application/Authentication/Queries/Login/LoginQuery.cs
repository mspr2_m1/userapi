﻿using ErrorOr;

using MediatR;

using UserApi.Application.Authentication.Authentication.Common;

namespace UserApi.Application.Authentication.Authentication.Queries.Login;

public record LoginQuery(
    string Email,
    string Password) : IRequest<ErrorOr<AuthenticationResult>>;