﻿using ErrorOr;

using MediatR;

using UserApi.Application.Authentication.Authentication.Common;

namespace UserApi.Application.Authentication.Authentication.Commands.Register;

public record RegisterCommand(
    string Email,
    string Password,
    string FirstName,
    string LastName,
    List<string> RoleIds) : IRequest<ErrorOr<AuthenticationResult>>;