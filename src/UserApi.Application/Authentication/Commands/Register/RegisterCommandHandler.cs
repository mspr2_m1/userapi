﻿using ErrorOr;

using MediatR;

using MsprApiUser.Application.Common.Interfaces.Authentication;

using UserApi.Application.Authentication.Authentication.Common;
using UserApi.Application.Common.Interfaces.Authentication;
using UserApi.Application.Common.Interfaces.Persistence;
using UserApi.Domain.RoleAggregate.ValueObjects;
using UserApi.Domain.UserAggregate;

using Errors = UserApi.Domain.Common.Errors.Errors;

namespace UserApi.Application.Authentication.Authentication.Commands.Register;

public class RegisterCommandHandler(
    IUserRepository userRepository,
    IRoleRepository roleRepository,
    IJwtTokenGenerator jwtTokenGenerator,
    IPasswordHasher passwordHasher)
    : IRequestHandler<RegisterCommand, ErrorOr<AuthenticationResult>>
{
    public async Task<ErrorOr<AuthenticationResult>> Handle(
        RegisterCommand command,
        CancellationToken cancellationToken)
    {
        if (await userRepository.GetByEmailAsync(command.Email) is not null)
        {
            return Errors.User.DuplicateEmail;
        }

        var roleIds = command.RoleIds.Select(r => RoleId.Create(r).Value).ToList();

        var roles = await roleRepository.GetByIdsAsync(roleIds);

        var user = User.Create(
            command.Email,
            passwordHasher.HashPassword(command.Password),
            command.FirstName,
            command.LastName,
            roles);

        await userRepository.AddAsync(user);

        return new AuthenticationResult(
            user,
            jwtTokenGenerator.GenerateToken(user));
    }
}