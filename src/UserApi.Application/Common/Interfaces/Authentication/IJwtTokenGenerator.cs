﻿using UserApi.Domain.RoleAggregate;
using UserApi.Domain.UserAggregate;

namespace UserApi.Application.Common.Interfaces.Authentication;

public interface IJwtTokenGenerator
{
    string GenerateToken(User user);
}