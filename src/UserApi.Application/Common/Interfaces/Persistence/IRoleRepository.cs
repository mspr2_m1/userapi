﻿using MsprApiUser.Application.Common.Interfaces.Persistence.Common;

using UserApi.Domain.RoleAggregate;
using UserApi.Domain.RoleAggregate.ValueObjects;

namespace UserApi.Application.Common.Interfaces.Persistence;

public interface IRoleRepository : IRepository<Role, RoleId>
{
    Task<List<Role>> GetByIdsAsync(List<RoleId> roleIds);
}