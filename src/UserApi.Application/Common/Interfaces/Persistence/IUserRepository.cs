﻿using MsprApiUser.Application.Common.Interfaces.Persistence.Common;

using UserApi.Domain.UserAggregate;
using UserApi.Domain.UserAggregate.ValueObjects;

namespace UserApi.Application.Common.Interfaces.Persistence;

public interface IUserRepository : IRepository<User, UserId>
{
    Task<User?> GetByEmailAsync(string email);
}