﻿using MapsterMapper;

using MediatR;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using UserApi.Application.Authentication.Authentication.Commands.Register;
using UserApi.Application.Authentication.Authentication.Queries.Login;
using UserApi.Contracts.Authentication;

namespace UserApi.Api.Controllers;

[Route("v1/[controller]")]
[AllowAnonymous]
public class AuthenticationController(ISender mediator, IMapper mapper) : ApiController
{
    [HttpPost]
    [Route("login")]
    public async Task<IActionResult> Login([FromBody] LoginRequest request)
    {
        var authResult = await mediator.Send(mapper.Map<LoginQuery>(request));

        return authResult.Match(
            result => Ok(mapper.Map<AuthenticationResponse>(result)),
            Problem);
    }

    [HttpPost]
    [Route("register")]
    public async Task<IActionResult> Register([FromBody] RegisterRequest request)
    {
        var authResult = await mediator.Send(mapper.Map<RegisterCommand>(request));

        return authResult.Match(
            result => Ok(mapper.Map<AuthenticationResponse>(result)),
            Problem);
    }
}