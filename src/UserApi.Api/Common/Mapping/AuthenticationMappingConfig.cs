﻿using Mapster;

using UserApi.Application.Authentication.Authentication.Commands.Register;
using UserApi.Application.Authentication.Authentication.Common;
using UserApi.Application.Authentication.Authentication.Queries.Login;
using UserApi.Contracts.Authentication;

namespace UserApi.Api.Common.Mapping;

public class AuthenticationMappingConfig : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.NewConfig<RegisterRequest, RegisterCommand>();

        config.NewConfig<LoginRequest, LoginQuery>();

        config.NewConfig<AuthenticationResult, AuthenticationResponse>()
            .Map(dest => dest, src => src.User);
    }
}