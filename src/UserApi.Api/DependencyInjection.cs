﻿using Microsoft.AspNetCore.Mvc.Infrastructure;

using UserApi.Api.Common.Errors;
using UserApi.Api.Common.Mapping;

namespace UserApi.Api;
public static class DependencyInjection
{
    public static IServiceCollection AddPresentation(this IServiceCollection services)
    {
        services.AddControllers();

        services
            .AddSingleton<ProblemDetailsFactory, UserApiProblemDetailsFactory>()
            .AddMappings();

        return services;
    }
}