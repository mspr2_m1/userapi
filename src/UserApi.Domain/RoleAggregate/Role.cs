﻿using UserApi.Domain.Common.Models;
using UserApi.Domain.RoleAggregate.ValueObjects;

namespace UserApi.Domain.RoleAggregate;

public sealed class Role : AggregateRoot<RoleId, Guid>
{
    public string Name { get; private set; }

    private Role(
        string name,
        RoleId? roleId = null)
        : base(roleId ?? RoleId.CreateUnique())
    {
        Name = name;
    }

    public static Role Create(
        string name)
    {
        return new Role(
            name);
    }

    public static Role Update(
        string name,
        RoleId roleId)
    {
        return new Role(
            name,
            roleId);
    }

    public static Role Delete(
        string name,
        RoleId roleId)
    {
        return new Role(
            name,
            roleId);
    }

#pragma warning disable CS8618
    private Role()
    {
    }
#pragma warning restore CS8618
}