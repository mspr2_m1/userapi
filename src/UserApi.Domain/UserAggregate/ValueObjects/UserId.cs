﻿using ErrorOr;

using UserApi.Domain.Common.Models.Identities;

using Errors = UserApi.Domain.Common.Errors.Errors;

namespace UserApi.Domain.UserAggregate.ValueObjects;

public sealed class UserId : AggregateRootId<Guid>
{
    private UserId(Guid value) : base(value)
    {
    }

    public static UserId Create(Guid userId) =>
        new(userId);

    public static UserId CreateUnique() =>
        new(Guid.NewGuid());

    public static ErrorOr<UserId> Create(string value)
    {
        if (!Guid.TryParse(value, out var guid))
        {
            return Errors.User.InvalidUserId;
        }

        return new UserId(guid);
    }
}