﻿using MediatR;

namespace UserApi.Domain.Common.Models;

public interface IDomainEvent : INotification
{
}