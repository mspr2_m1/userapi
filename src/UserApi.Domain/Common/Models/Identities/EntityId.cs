﻿namespace UserApi.Domain.Common.Models.Identities;

public abstract class EntityId<TId>(TId value) : ValueObject
{
    public TId Value { get; } = value;

    public override IEnumerable<object?> GetEqualityComponents()
    {
        yield return Value;
    }

    public override string? ToString() => Value?.ToString() ?? base.ToString();
}