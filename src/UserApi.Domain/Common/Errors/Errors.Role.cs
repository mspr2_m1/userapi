﻿using ErrorOr;

namespace UserApi.Domain.Common.Errors;

public static partial class Errors
{
    public static class Role
    {
        public static Error InvalidRoleId => Error.Conflict(
            code: "Role.InvalidRoleId",
            description: "RoleId is Invalid");
    }
}