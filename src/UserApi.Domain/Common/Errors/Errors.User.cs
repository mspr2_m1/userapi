﻿using ErrorOr;

namespace UserApi.Domain.Common.Errors;

public static partial class Errors
{
    public static class User
    {
        public static Error DuplicateEmail => Error.Conflict(
            code: "User.DuplicateEmail",
            description: "Email is already used");

        public static Error InvalidUserId => Error.Conflict(
            code: "User.InvalidUserId",
            description: "Invalid user Id");

        public static Error UserNotFound => Error.NotFound(
            code: "User.UserNotFound",
            description: "User not found");
    }
}