﻿using FluentAssertions;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Moq;

using UserApi.Domain.UserAggregate;
using UserApi.Domain.UserAggregate.ValueObjects;
using UserApi.Infrastructure.Persistence;
using UserApi.Infrastructure.Persistence.Interceptors;
using UserApi.Infrastructure.Persistence.Repositories;

using Xunit;

namespace UserApi.Infrastructure.UnitTests;

public class UserRepositoryTests
{
    private readonly DbContextOptions<UserApiDbContext> _dbContextOptions;
    private readonly PublishDomainEventsInterceptor _publishDomainEventsInterceptor;
    private readonly List<User> _users = [];

    public UserRepositoryTests()
    {
        for (int i = 0; i < 4; i++)
        {
            _users.Add(User.Create(
                email: $"email{i}",
                password: $"password{i}",
                firstName: $"firstName{i}",
                lastName: $"lastName{i}"));
        }

        _publishDomainEventsInterceptor = new PublishDomainEventsInterceptor(
            new Mock<IPublisher>().Object);

        _dbContextOptions = new DbContextOptionsBuilder<UserApiDbContext>()
            .UseInMemoryDatabase(databaseName: "TestDatabase")
            .Options;

        var userApiDbContext = new UserApiDbContext(
            _dbContextOptions,
            _publishDomainEventsInterceptor);

        SeedMemoryDatabase(userApiDbContext);
    }

    [Fact]
    public async Task GetAllAsync_Should_Returns_All_Roles()
    {
        using var userApiDbContext = new UserApiDbContext(
            _dbContextOptions,
            _publishDomainEventsInterceptor);

        var userRepository = new UserRepository(userApiDbContext);

        var result = await userRepository.GetAllAsync();

        result.Should().NotBeNull();
        result.Should().HaveCount(4);
        result[0].Email.Should().BeEquivalentTo("email0");
        result[1].Email.Should().BeEquivalentTo("email1");
        result[2].Email.Should().BeEquivalentTo("email2");
        result[3].Email.Should().BeEquivalentTo("email3");
    }

    [Fact]
    public async Task GetByIdAsync_Should_Returns_User_By_Id()
    {
        using var userApiDbContext = new UserApiDbContext(
            _dbContextOptions,
            _publishDomainEventsInterceptor);

        var userRepository = new UserRepository(userApiDbContext);

        var result = await userRepository.GetByIdAsync((UserId)_users[1].Id);

        result.Should().NotBeNull();
        result?.Email.Should().BeEquivalentTo("email1");
    }

    [Fact]
    public async Task AddAsync_Should_Add_User_Correctly()
    {
        using var userApiDbContext = new UserApiDbContext(
            _dbContextOptions,
            _publishDomainEventsInterceptor);

        var userRepository = new UserRepository(userApiDbContext);

        var user = User.Create(
                email: $"emailCreated",
                password: $"passwordCreated",
                firstName: $"firstNameCreated",
                lastName: $"lastNameCreated");

        await userRepository.AddAsync(user);
        var result = await userRepository.GetByIdAsync((UserId)user.Id);

        result.Should().NotBeNull();
        result.Should().BeEquivalentTo(user);
    }

    [Fact]
    public async Task UpdateAsync_Should_Update_User_Correctly()
    {
        using var userApiDbContext = new UserApiDbContext(
            _dbContextOptions,
            _publishDomainEventsInterceptor);

        var userRepository = new UserRepository(userApiDbContext);

        var userToUpdate = User.Update(
            userId: UserId.Create(_users[1].Id.ToString()!).Value,
            email: $"emailUpdated",
            password: $"passwordUpdated",
            firstName: $"firstNameUpdated",
            lastName: $"lastNameUpdated");

        await userRepository.UpdateAsync(userToUpdate);
        var result = await userRepository.GetByIdAsync((UserId)_users[1].Id);

        result.Should().NotBeNull();
        result?.Should().BeEquivalentTo(userToUpdate);
    }

    [Fact]
    public async Task DeleteAsync_Should_Delete_User_Correctly()
    {
        using var userApiDbContext = new UserApiDbContext(
            _dbContextOptions,
            _publishDomainEventsInterceptor);

        var userRepository = new UserRepository(userApiDbContext);

        var userToUpdate = User.Delete(
            userId: UserId.Create(_users[1].Id.ToString()!).Value,
            email: $"email1",
            password: $"email1",
            firstName: $"email1",
            lastName: $"email1");

        await userRepository.DeleteAsync(userToUpdate);
        var result = await userRepository.GetByIdAsync((UserId)_users[1].Id);

        result.Should().BeNull();
    }

    private void SeedMemoryDatabase(UserApiDbContext userApiDbContext)
    {
        userApiDbContext.Database.EnsureDeleted();
        userApiDbContext.Users.AddRange(_users);
        userApiDbContext.SaveChanges();
    }
}