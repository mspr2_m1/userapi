﻿using FluentAssertions;

using MediatR;

using Microsoft.EntityFrameworkCore;

using Moq;

using UserApi.Domain.RoleAggregate;
using UserApi.Domain.RoleAggregate.ValueObjects;
using UserApi.Infrastructure.Persistence;
using UserApi.Infrastructure.Persistence.Interceptors;
using UserApi.Infrastructure.Persistence.Repositories;

using Xunit;

namespace UserApi.Infrastructure.UnitTests;

public class RoleRepositoryTests
{
    private readonly DbContextOptions<UserApiDbContext> _dbContextOptions;
    private readonly PublishDomainEventsInterceptor _publishDomainEventsInterceptor;
    private readonly List<Role> _roles = [];

    public RoleRepositoryTests()
    {
        for (int i = 0; i < 4; i++)
        {
            _roles.Add(Role.Create($"role{i}"));
        }

        _publishDomainEventsInterceptor = new PublishDomainEventsInterceptor(
            new Mock<IPublisher>().Object);

        _dbContextOptions = new DbContextOptionsBuilder<UserApiDbContext>()
            .UseInMemoryDatabase(databaseName: "TestDatabase")
            .Options;

        var userApiDbContext = new UserApiDbContext(
            _dbContextOptions,
            _publishDomainEventsInterceptor);

        SeedMemoryDatabase(userApiDbContext);
    }

    [Fact]
    public async Task GetAllAsync_Should_Returns_All_Roles()
    {
        using var userApiDbContext = new UserApiDbContext(
            _dbContextOptions,
            _publishDomainEventsInterceptor);

        var roleRepository = new RoleRepository(userApiDbContext);

        var result = await roleRepository.GetAllAsync();

        result.Should().NotBeNull();
        result.Should().HaveCount(4);
        result[0].Name.Should().BeEquivalentTo("role0");
        result[1].Name.Should().BeEquivalentTo("role1");
        result[2].Name.Should().BeEquivalentTo("role2");
        result[3].Name.Should().BeEquivalentTo("role3");
    }

    [Fact]
    public async Task GetByIdAsync_Should_Returns_Role_By_Id()
    {
        using var userApiDbContext = new UserApiDbContext(
            _dbContextOptions,
            _publishDomainEventsInterceptor);

        var roleRepository = new RoleRepository(userApiDbContext);

        var result = await roleRepository.GetByIdAsync((RoleId)_roles[1].Id);

        result.Should().NotBeNull();
        result?.Name.Should().BeEquivalentTo("role1");
    }

    [Fact]
    public async Task AddAsync_Should_Add_Role_Correctly()
    {
        using var userApiDbContext = new UserApiDbContext(
            _dbContextOptions,
            _publishDomainEventsInterceptor);

        var roleRepository = new RoleRepository(userApiDbContext);

        var role = Role.Create("roleCreated");

        await roleRepository.AddAsync(role);
        var result = await roleRepository.GetByIdAsync((RoleId)role.Id);

        result.Should().NotBeNull();
        result.Should().BeEquivalentTo(role);
    }

    [Fact]
    public async Task UpdateAsync_Should_Update_Role_Correctly()
    {
        using var userApiDbContext = new UserApiDbContext(
            _dbContextOptions,
            _publishDomainEventsInterceptor);

        var roleRepository = new RoleRepository(userApiDbContext);

        var roleToUpdate = Role.Update(
            roleId: RoleId.Create(_roles[1].Id.ToString()!).Value,
            name: $"roleUpdated");

        await roleRepository.UpdateAsync(roleToUpdate);
        var result = await roleRepository.GetByIdAsync((RoleId)_roles[1].Id);

        result.Should().NotBeNull();
        result?.Should().BeEquivalentTo(roleToUpdate);
    }

    [Fact]
    public async Task DeleteAsync_Should_Delete_Role_Correctly()
    {
        using var userApiDbContext = new UserApiDbContext(
            _dbContextOptions,
            _publishDomainEventsInterceptor);

        var roleRepository = new RoleRepository(userApiDbContext);

        var roleToUpdate = Role.Delete(
            roleId: RoleId.Create(_roles[1].Id.ToString()!).Value,
            name: $"roleDeleted");

        await roleRepository.DeleteAsync(roleToUpdate);
        var result = await roleRepository.GetByIdAsync((RoleId)_roles[1].Id);

        result.Should().BeNull();
    }

    private void SeedMemoryDatabase(UserApiDbContext userApiDbContext)
    {
        userApiDbContext.Database.EnsureDeleted();
        userApiDbContext.Roles.AddRange(_roles);
        userApiDbContext.SaveChanges();
    }
}