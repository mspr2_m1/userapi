###> VARIABLES - start ###
DOCKER_COMP = docker compose
DOCKER_EXEC = docker exec -t userapi-app
###> VARIABLES - end ###

###> DOCKER COMPOSE - start ###
build:
	@$(DOCKER_COMP) build

up:
	@$(DOCKER_COMP) up -d

down:
	@$(DOCKER_COMP) down --remove-orphans

restart: down up

logs:
	@$(DOCKER_COMP) logs -f
###> DOCKER COMPOSE - end ###

###> TOOLS CONFIGURATION - start ###
term:
	@${DOCKER_EXEC} bash
###> TOOLS CONFIGURATION - end ###
