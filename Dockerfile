FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build

WORKDIR /app

COPY ./src/. ./src
COPY ./Directory.Build.Props ./src

RUN dotnet restore ./src/UserApi.Api/UserApi.Api.csproj

COPY ./src ./src

RUN dotnet publish ./src/UserApi.Api -c Release -o /publish

FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS runtime
WORKDIR /app
COPY --from=build /publish ./

EXPOSE 80

ENV ASPNETCORE_URLS=http://*:80
ENTRYPOINT ["dotnet", "UserApi.Api.dll"]